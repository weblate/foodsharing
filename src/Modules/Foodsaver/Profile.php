<?php

namespace Foodsharing\Modules\Foodsaver;

class Profile
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $avatar;

	/**
	 * @var int
	 */
	public $sleepStatus;
}
