# Beta Testing

Alle Neuerungen und Fehlerbehebungen, die auf Foodsharing.de online gehen, durchlaufen vorab eine Testphase auf [beta.foodsharing.de](https://beta.foodsharing.de/). Das Beta-Testing dort ist extrem wichtig für uns, da nur so vermieden werden kann, dass wir Fehler auf die Gemeinschaft loslassen und unter Umständen die Seite, die Datenbank oder sogar den ganzen Ablauf beschädigen. Ihr könnt durch frühes Finden und Melden von Fehlern allen Nutzern (und insbesondere auch den Programmierer*innen) das Leben angenehmer machen. Keiner will nach dem Release einer neuen Live-Version die Nacht durchmachen, wenn auf einmal die Fehler dann erst deutlich werden.

Praktischer Nebeneffekt: Auf der Beta-Seite könnt ihr nützliche Neuerungen teilweise schon viele Wochen früher nutzen.

## So unterstützt ihr uns beim Testen

1. Die gesamte Kommunikation rund ums Testen findet im Messenger Slack statt. Registriere dich dafür bitte unter [slackin.yunity.org](https://slackin.yunity.org/) (das geht schnell und kostenlos) und installier anschließend die App (gibt’s auf [slack.com](https://slack.com/intl/de-de/download) für Windows, macOS, Linux, iOS und Android). Auf Slack gibt es für verschiedene Themen unterschiedliche Channels, der wichtigste für dich heißt **#foodsharing-beta** – hier bitte beitreten. Im Beta-Kanal kommunizieren wir auf Deutsch, an denen meisten anderen Orten auf Englisch.
2. Im besagten Channel melden sich Entwickler*innen mit Testaufträgen (im Normalfall immer dann, wenn gerade eine Änderung an der Seite fertig geworden ist). Dein Job: Die beschriebene Funktion auf [beta.foodsharing.de](https://beta.foodsharing.de/) ausprobieren und Rückmeldung geben: Wenn alles klappt wie erwartet, reicht ein kurzer Satz. Sollte es Probleme geben, beschreibe bitte, was du getan hast und was nicht funktioniert hat. **Wichtig:** Gib nach Möglichkeit zu _allen_ Testaufträgen Rückmeldung, auch wenn das manchmal ein bisschen Arbeit bedeutet – nur so bekommen wir ein vollständiges Bild und finden alle Probleme.
3. Du kannst [beta.foodsharing.de](https://beta.foodsharing.de/) ausschließlich für Testaufträge oder auch dauerhaft nutzen. Beide Versionen der Seite greifen auf die gleichen Daten zu und bieten (bis auf neue Features und Fehlerbehebungen) die gleichen Funktionen. Aber: Obwohl wir grundsätzlich versuchen, nichts kaputt zu machen, können sich Fehler einschleichen. In einigen Fällen (und gerade dann, wenn etwas nicht wie gewollt funktioniert) ist es daher sinnvoll, auf foodsharing.de auszuweichen.


## Und noch ein paar wichtige Punkte

1. Damit es nicht unübersichtlich wird, arbeiten wir in Slack mit Threads: Über das Sprechblasensymbol könnt ihr direkt auf eine Nachricht antworten, also zum Beispiel auf einen Testauftrag. Auf diese Weise sind alle Informationen an einer Stelle gebündelt und schnell auffindbar. ([Eine genaue Erklärung findet ihr hier](https://slack.com/intl/de-de/help/articles/115000769927-Threads-benutzen-um-Diskussionen-zu-organisieren-).)
2. Den Kanal #foodsharing-beta könnt ihr auch nutzen, um auf Probleme aufmerksam zu machen, für die es keinen Testauftrag gibt oder die ihr keinem Auftrag zuordnen könnt. Wichtigstes Kriterium für eine Fehlermeldung: Auf beta.foodsharing.de ist es kaputt, auf foodsharing.de nicht.
3. Unter jedem Testauftrag findet sich ein Link zu gitlab.com, dem Portal, auf dem die Entwicklung koordiniert wird. Hier finden sich oft noch mehr Informationen zur jeweiligen Änderung.

## Noch tiefer einsteigen?

Auch über das Bearbeiten von Testaufgaben hinaus könnt ihr uns unterstützen, zum Beispiel durch das Anlegen und Betreuen von Fehlerberichten, sogenannten Issues. Mit einem kostenlosen Account bei Gitlab könnt ihr solche Issues für das [Foodsharing-Projekt](https://gitlab.com/foodsharing-dev/foodsharing/issues) anlegen. Es gibt verschiedene Templates, mit denen diese Meldungen strukturiert werden können (und sollten). Wichtig: Die Issues sind idealerweise so ausführlich, dass auch Menschen sie nachvollziehen können, die (noch) keine aktiven Foodsaver sind. Hilfreich sind Screenshots und Schritt-für-Schritt-Anleitungen, mit denen sich Fehler reproduzieren lassen.

Zu guter Letzt: Herzlichen Dank, dass Ihr mitmacht! Sowohl IT als auch alle Nutzer*innen der Seite sind Euch sehr dankbar (auch wenn Letztere manchmal nicht wissen, wieso).
